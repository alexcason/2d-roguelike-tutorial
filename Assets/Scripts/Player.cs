﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Player : MovingObject {

	public int wallDamage = 1;
	public int pointsPerFood = 10;
	public int pointsPerSoda = 20;
	public float restartLevelDelay = 1f;
	public Text foodText;
	public AudioClip moveSound1;
	public AudioClip moveSound2;
	public AudioClip eatSound1;
	public AudioClip eatSound2;
	public AudioClip drinkSound1;
	public AudioClip drinkSound2;
	public AudioClip gameOverSound;

	private Animator animator;
	private int food;

	protected override void Start()
	{
		animator = GetComponent<Animator> ();

		food = GameManager.instance.playerFoodPoints;

		SetFoodText (food);

		base.Start ();
	}

	private void OnDisable()
	{
		GameManager.instance.playerFoodPoints = food;
	}

	void Update()
	{
		// check if player turn - skip if not
		if (!GameManager.instance.playersTurn)
		{
			return;
		}

		int horizontal = 0;
		int vertical = 0;

		horizontal = (int)Input.GetAxisRaw (Constants.Input.HORIZONTAL);
		vertical = (int)Input.GetAxisRaw (Constants.Input.VERTICAL);

		// prevent diagonal moves
		if (horizontal != 0)
		{
			vertical = 0;
		}

		// if input detected then attempt move
		if (horizontal != 0 || vertical != 0)
		{
			AttemptMove<Wall> (horizontal, vertical);
		}
	}

	protected override void AttemptMove <T> (int xDir, int yDir)
	{
		food--;
		SetFoodText (food);

		base.AttemptMove <T> (xDir, yDir);

		RaycastHit2D hit;

		if (Move (xDir, yDir, out hit))
	    {
			SoundManager.instance.RandomizeSfx(moveSound1, moveSound2);
		}

		CheckIfGameOver ();

		GameManager.instance.playersTurn = false;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == Constants.Tag.EXIT)
		{
			Invoke (Constants.Game.RESTART, restartLevelDelay);
			enabled = false;
		}
		else if (other.tag == Constants.Tag.FOOD)
		{
			food += pointsPerFood;
			SetFoodText(food, pointsPerFood);
			SoundManager.instance.RandomizeSfx(eatSound1, eatSound1);
			other.gameObject.SetActive (false);
		}
		else if (other.tag == Constants.Tag.SODA)
		{
			food += pointsPerSoda;
			SetFoodText(food, pointsPerSoda);
			SoundManager.instance.RandomizeSfx(drinkSound1, drinkSound2);
			other.gameObject.SetActive (false);
		}
	}

	protected override void OnCantMove<T>(T component)
	{
		Wall hitWall = component as Wall;
		hitWall.DamageWall (wallDamage);
		animator.SetTrigger (Constants.Trigger.PLAYER_CHOP);
	}

	private void Restart()
	{
		Application.LoadLevel (Application.loadedLevel);
	}

	public void LoseFood(int loss)
	{
		animator.SetTrigger (Constants.Trigger.PLAYER_HIT);
		food -= loss;
		SetFoodText(food, -loss);

		CheckIfGameOver ();
	}

	private void CheckIfGameOver()
	{
		if (food <= 0)
		{
			SoundManager.instance.PlaySingle(gameOverSound);
			SoundManager.instance.musicSource.Stop();
			GameManager.instance.GameOver();
		}
	}

	private void SetFoodText(int food, int change = 0)
	{
		string foodScoreText = "Food: " + food;
		string foodChangeText = "";

		if (change != 0)
		{
			string changeSymbol = "";

			if (change > 0)
			{
				changeSymbol = "+";
			}

			foodChangeText = changeSymbol + change + " ";
		}

		foodText.text = foodChangeText + foodScoreText;
	}
}
