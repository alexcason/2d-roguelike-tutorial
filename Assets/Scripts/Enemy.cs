﻿using UnityEngine;
using System.Collections;

public class Enemy : MovingObject {

	public int playerDamage;
	public AudioClip enemyAttack1;
	public AudioClip enemyAttack2;

	private Animator animator;
	private Transform target;
	private bool skipMove;

	protected override void Start()
	{
		GameManager.instance.AddEnemyToList (this);
		animator = GetComponent<Animator> ();
		target = GameObject.FindGameObjectWithTag (Constants.Tag.PLAYER).transform;
		base.Start ();
	}

	protected override void AttemptMove<T> (int xDir, int yDir)
	{
		// only move every other turn
		if (skipMove)
		{
			skipMove = false;

			return;
		}

		base.AttemptMove <T> (xDir, yDir);

		skipMove = true;
	}

	public void MoveEnemy()
	{
		int xDir = 0;
		int yDir = 0;

		// are x coordinates approximately the same and therefore the player and enemy are in the same column
		if (Mathf.Abs (target.position.x - transform.position.x) < float.Epsilon)
		{
			yDir = target.position.y > transform.position.y ? 1 : -1;
		}
		else
		{
			xDir = target.position.x > transform.position.x ? 1 : -1;
		}

		AttemptMove <Player> (xDir, yDir);
	}

	protected override void OnCantMove <T> (T component)
	{
		Player hitPlayer = component as Player;

		animator.SetTrigger (Constants.Trigger.ENEMY_ATTACK);

		// the player has been hit so they are damaged
		hitPlayer.LoseFood (playerDamage);

		SoundManager.instance.RandomizeSfx (enemyAttack1, enemyAttack2);
	}

}
