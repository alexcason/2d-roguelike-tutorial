using System;

namespace Constants
{
	public static class Tag
	{
		public const string EXIT = "Exit";
		public const string FOOD = "Food";
		public const string SODA = "Soda";
		public const string PLAYER = "Player";
	}
}
