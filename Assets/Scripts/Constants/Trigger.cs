using System;

namespace Constants
{
	public static class Trigger
	{
		public const string PLAYER_CHOP = "playerChop";
		public const string PLAYER_HIT = "playerHit";
		public const string ENEMY_ATTACK = "enemyAttack";
	}
}

